#SoftwareDevtProject_12
This is a our software development group project for group 12 2024.

# Overview
ShopNet is a web platform for local businessses to connect with their customers and offer online ordering, delivery, and/or booking services.Both businesses and customers can create accounts(either business accounts or customer accounts.)


## Functional Specifications


    ### User Management
    
    - Customers and business owners can register and create an account, based on two categories;
        Business owners account, or customer account.

    - Once they have signed up, with a correct email, they will receive a confirmation email, welcoming them as businesses/customers.
    
    - Customers and business owners can log in and log out of their accounts.
    
    - Customers and business owners can reset their passwords if forgotten, using their emails.


    ### Product and Service Management
    - Business owners can upload logos, photos, and/or videos to showcase their offerings.

    - Business owners can manage products, orders on their products, and customers

    - They can set pricing and availability for their offerings.

    - Customers can browse products by name, category or business.
    
    - Customers can search for products by name, category or business.

    - Customers can view detailed information about each product.

    - They can add products to their shopping cart.
    
    - They can remove products from their shopping cart.
    
    - They can view their shopping cart and proceed to checkout.
    
    - They can view their order history and folow up on their status(Pending, Order confirmed, On the way, Delivered).

    ### Order Management

    - Customers can place orders for products.
    
    - Customers can view the status of their orders.

    -Businesses can view their customers(based on successful orders) and total earnings

    -Businesses can change order status of their customers' goods.
     

    ### Admin Panel

    - Admins can add, edit, and delete products, categories.

    - Admins can view and manage user accounts(both businesses and customers).

    - Admins can view and manage orders.
    
    - Admins can manage platform settings and configurations.


## Technological Specifications

    ### Frontend

    - HTML, CSS, JavaScript for the user interface, as well as Bootstrap CSS for UI fonts and icons.

    ### Backend

    - Django framework for server-side development.

    - SQLite database for data storage.

    ### Authentication and Authorization
    - Django’s built-in authentication system for user authentication.

    - Role-based access control for admin functionality.

    ### Deployment
    - Using pythonanywhere.com for optimal and free hosting


    ### Testing
    - Unit tests for backend logic using Django’s testing framework.

    - Integration tests for frontend and backend interactions.
