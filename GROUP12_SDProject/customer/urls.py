
from django.contrib.auth import views as auth_views
from django.urls import path
from .views import *
from django.contrib.auth.views import LoginView,LogoutView

urlpatterns = [
    path('',home_view,name='home'),
    path('afterlogin/', afterlogin_view,name='afterlogin'),
    path('logout/', LogoutView.as_view(template_name='customer/logout.html'),name='logout'),
    path('aboutus/', aboutus_view),
    path('terms/', terms_conditions_view),
    path('faqs/', FAQs_view),
    path('reviews/', reviews_view),
    path('contactus/', contactus_view,name='contactus'),
    path('search/', search_view,name='search'),
    path('send-feedback/', send_feedback_view,name='send-feedback'),
    path('customersignup/', customer_signup_view),
    path('customerlogin/', LoginView.as_view(template_name='customer/customerlogin.html'),name='customerlogin'),
    path('customersignup/customerlogin/', LoginView.as_view(template_name='customer/customerlogin.html'),name='customerlogin'),
    path('customer-home/', customer_home_view,name='customer-home'),
    path('product_detail/<int:pk>', product_detail,name='product-detail'),
    path('my-order/', my_order_view,name='my-order'),
    path('my-profile/', my_profile_view,name='my-profile'),
    path('edit-profile/', edit_profile_view,name='edit-profile'),
    path('edit-profile/my-profile/', my_profile_view,name='my-profile'),
    path('download-invoice/<int:orderID>/<int:productID>', download_invoice_view,name='download-invoice'),
    path('add-to-cart/<int:pk>', add_to_cart_view,name='add-to-cart'),
    path('cart/', cart_view,name='cart'),
    path('remove-from-cart/<int:pk>', remove_from_cart_view,name='remove-from-cart'),
    path('customer-address/', customer_address_view,name='customer-address'),
    path('payment-success/', payment_success_view,name='payment-success'),
    path('product/<int:product_id>/add_review/', add_review, name='add_review'),
    path('category/<str:title>/', category, name="category"),
    path('business/<int:pk>/', business_detail, name='business-detail'),
    path('customer-password-reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('customer-password-reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('customer-password-reset/confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('customer-password-reset/complete/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]