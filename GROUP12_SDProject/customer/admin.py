from django.contrib import admin
from .models import Customer,Orders,Feedback,Review
from django.utils.html import format_html


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('get_name', 'get_email','e_mail', 'address', 'mobile', 'created_at','profile_pic_image')
    search_fields = ('user__first_name', 'user__last_name', 'e_mail')
    list_filter = ('created_at',)

    def get_email(self, obj):
        return obj.user.email
    get_email.short_description = 'Email'

    def profile_pic_image(self, obj):
        return format_html('<img src="/static{}" style="width: 50px; height:50px;"/>', obj.profile_pic.url) if obj.profile_pic else ''
    profile_pic_image.short_description = 'Profile Picture'

admin.site.register(Customer, CustomerAdmin)

class OrderAdmin(admin.ModelAdmin):
    list_display = ('customer', 'product', 'email', 'address', 'mobile', 'order_date', 'status')
    search_fields = ('customer__user__first_name', 'customer__user__last_name', 'email')
    list_filter = ('status', 'order_date')
admin.site.register(Orders, OrderAdmin)

class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('name', 'feedback', 'date')
    search_fields = ('name', 'feedback')
    list_filter = ('date',)
admin.site.register(Feedback, FeedbackAdmin)

class ReviewAdmin(admin.ModelAdmin):
    list_display = ('product', 'reviewer_name', 'reviewer_email','rating', 'created_at')  # Fields to display in the list view
    list_filter = ('product', 'rating')  # Add filters for product and rating
    search_fields = ('reviewer_name', 'review_text')  # Add search functionality for reviewer name and review text
    readonly_fields = ('created_at',)  # Mark created_at field as read-only
    # Optionally, customize the form layout or behavior
    fieldsets = (
        (None, {
            'fields': ('product', 'reviewer_name', 'reviewer_email', 'rating', 'review_text')
        }),
        ('Date Information', {
            'fields': ('created_at',),
            'classes': ('collapse',)  # Hide this section by default
        }),
    )
admin.site.register(Review,ReviewAdmin)

