# Generated by Django 4.2.5 on 2024-05-06 12:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0004_review'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='is_email_verified',
            field=models.BooleanField(default=False),
        ),
    ]
