import io
from xhtml2pdf import pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from django.http import HttpResponseRedirect,HttpResponse
from django.core.mail import send_mail
from django.db.models import Q
from django.contrib.auth.models import Group
from django.shortcuts import render,redirect,get_object_or_404
from django.contrib.auth.decorators import login_required,user_passes_test
# from . import forms,models
from business import models as b_models
from django.contrib import messages
from django.conf import settings
from business.views import is_business
from customer.models import *
from customer.forms import *
from customer.views import *

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

sender_email = "tuhaiseugene@gmail.com"
sender_password = "xfko grfs unsc gycc"
#---------------------------------------------------------------------------------
#------------------------ CUSTOMER RELATED VIEWS START ------------------------------
#---------------------------------------------------------------------------------

def send_message(recipient_email,firstname):
            message = MIMEMultipart()
            message['From'] = sender_email
            message['To'] = recipient_email
            message['Subject']  = "Email Confirmed!!!"
            body = f"""
            Warm Greetings, {firstname},
    
    
            WELCOME TO SHOPNET

            This is to congragulate you on successfully 
            creating an account with us as a customer user.
        
            If this was not by you please ignore this.


            THE SHOPNET TEAM
            """
            message.attach(MIMEText(body, "plain"))
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
            server.login(sender_email, sender_password)
            server.sendmail(sender_email, recipient_email, message.as_string())
            server.quit()
            return 1


#view for signing as a customer
def customer_signup_view(request):
    userForm=CustomerUserForm()
    customerForm=CustomerForm()
    mydict={'userForm':userForm,'customerForm':customerForm}
    if request.method=='POST':
        userForm=CustomerUserForm(request.POST)
        customerForm=CustomerForm(request.POST,request.FILES)
        if userForm.is_valid() and customerForm.is_valid():
            user=userForm.save()
            user.set_password(user.password)
            user.save()
            customer=customerForm.save(commit=False)
            customer.user=user
            customer.save()
            send_message(user.email,user.first_name)
            my_customer_group = Group.objects.get_or_create(name='CUSTOMER')
            my_customer_group[0].user_set.add(user)
            return HttpResponseRedirect('customerlogin')
        else:
            messages.error(request,'Sign Up Failed!! Try again.')
    return render(request,'customer/customersignup.html',context=mydict)

#index page view
def home_view(request):
    products=b_models.Product.objects.all()
    category = b_models.Category.objects.all()
    word="On sale"
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        counter=product_ids.split('|')
        product_count_in_cart=len(set(counter))
    else:
        product_count_in_cart=0
    if request.user.is_authenticated:
        return HttpResponseRedirect('afterlogin')
    return render(request,'customer/index.html',{'categories': category,'products':products,'product_count_in_cart':product_count_in_cart,'word':word})

#category page
def category(request, title):
    category = b_models.Category.objects.filter(title=title).first()
    categories = b_models.Category.objects.all()
    products = b_models.Product.objects.filter(category=category)
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        counter=product_ids.split('|')
        product_count_in_cart=len(set(counter))
    else:
        product_count_in_cart=0
    return render(request,'customer/category.html',{'categories': categories, 'products':products,'product_count_in_cart':product_count_in_cart})

#when u want to view the product individually
def product_detail(request,pk):
    product=get_object_or_404(models.Product, pk=pk)
    context={'product':product}
    return render(request,'customer/single_product_detail.html',context)

@login_required(login_url='customerlogin')
def business_detail(request, pk):
    print("Business ID:", pk)
    business=get_object_or_404(b_models.Business, pk=pk)
    category = b_models.Category.objects.all()
    products = business.product_set.all()  # Assuming you have related name in Product model
    context = {'business': business, 'products': products,'categories': category}
    return render(request, 'customer/business_detail.html', context)

# help with login
def afterlogin_view(request):
    if is_customer(request.user):
        return redirect('customer-home')
    elif is_business(request.user):
        return redirect('business-dashboard')
    else:
        return redirect('home')


#-----------for checking if user is customer
def is_customer(user):
    return user.groups.filter(name='CUSTOMER').exists()


# for the search bar
def search_view(request):
    # whatever user write in search box we get in query
    query = request.GET.get('query')
    products = models.Product.objects.filter(
        Q(name__icontains=query) |                      # Search by product name
        Q(business__user__username__icontains=query) |  # Search by business owner's username
        Q(category__title__icontains=query)             # Search by category title
    ).distinct()
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        counter=product_ids.split('|')
        product_count_in_cart=len(set(counter))
    else:
        product_count_in_cart=0

    # word variable will be shown in html when user click on search button
    category = b_models.Category.objects.all()
    word="Searched Result :"
    context = {'products':products,'word':word,'product_count_in_cart':product_count_in_cart,'categories':category}
    if request.user.is_authenticated:
        return render(request,'customer/index.html',context)
    return render(request,'customer/index.html',context)


# any one can add product to cart, no need of signin
@login_required(login_url='customerlogin')
def add_to_cart_view(request,pk):
    products=models.Product.objects.all()
    category = b_models.Category.objects.all()
    #for cart counter, fetching product IDs added by customer from cookies
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        counter=product_ids.split('|')
        product_count_in_cart=len(set(counter))
    else:
        product_count_in_cart=1

    response = render(request, 'customer/index.html',{'categories':category,'products':products,'product_count_in_cart':product_count_in_cart})

    #adding product ID to cookies
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        if product_ids=="":
            product_ids=str(pk)
        else:
            product_ids=product_ids+"|"+str(pk)
        response.set_cookie('product_ids', product_ids)
    else:
        response.set_cookie('product_ids', pk)

    product=models.Product.objects.get(id=pk)
    messages.info(request, product.name + ' added to cart successfully!')

    return response



# for checkout of cart
@login_required(login_url='customerlogin')
def cart_view(request):
    #for cart counter
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        counter=product_ids.split('|')
        product_count_in_cart=len(set(counter))
    else:
        product_count_in_cart=0

    # fetching product details from database whose ID is present in cookie
    products=None
    total=0
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        if product_ids != "":
            product_id_in_cart=product_ids.split('|')
            products=models.Product.objects.all().filter(id__in = product_id_in_cart)

            #for total price shown in cart
            for p in products:
                total=total+p.price
    return render(request,'customer/cart.html',{'products':products,'total':total,'product_count_in_cart':product_count_in_cart})

@login_required(login_url='customerlogin')
def remove_from_cart_view(request,pk):
    #for counter in cart
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        counter=product_ids.split('|')
        product_count_in_cart=len(set(counter))
    else:
        product_count_in_cart=0

    # removing product id from cookie
    total=0
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        product_id_in_cart=product_ids.split('|')
        product_id_in_cart=list(set(product_id_in_cart))
        product_id_in_cart.remove(str(pk))
        products=models.Product.objects.all().filter(id__in = product_id_in_cart)
        #for total price shown in cart after removing product
        for p in products:
            total=total+p.price

        #  for update coookie value after removing product id in cart
        value=""
        for i in range(len(product_id_in_cart)):
            if i==0:
                value=value+product_id_in_cart[0]
            else:
                value=value+"|"+product_id_in_cart[i]
        response = render(request, 'customer/cart.html',{'products':products,'total':total,'product_count_in_cart':product_count_in_cart})
        if value=="":
            response.delete_cookie('product_ids')
        response.set_cookie('product_ids',value)
        return response

#for sending feedbacks
def send_feedback_view(request):
    feedbackForm=forms.FeedbackForm()
    if request.method == 'POST':
        feedbackForm = forms.FeedbackForm(request.POST)
        if feedbackForm.is_valid():
            feedbackForm.save()
            return render(request, 'customer/feedback_sent.html')
    return render(request, 'customer/send_feedback.html', {'feedbackForm':feedbackForm})


@login_required(login_url='customerlogin')
@user_passes_test(is_customer)
def customer_home_view(request):
    products=b_models.Product.objects.all()
    category = b_models.Category.objects.all()
    word="On Sale!!"
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        counter=product_ids.split('|')
        product_count_in_cart=len(set(counter))
    else:
        product_count_in_cart=0
    return render(request,'customer/index.html',{'categories':category,'products':products,'product_count_in_cart':product_count_in_cart,'word':word})



# shipment address before placing order
@login_required(login_url='customerlogin')
def customer_address_view(request):
    # this is for checking whether product is present in cart or not
    # if there is no product in cart we will not show address form
    product_in_cart=False
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        if product_ids != "":
            product_in_cart=True
    #for counter in cart
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        counter=product_ids.split('|')
        product_count_in_cart=len(set(counter))
    else:
        product_count_in_cart=0

    addressForm = AddressForm()
    if request.method == 'POST':
        addressForm = AddressForm(request.POST)
        if addressForm.is_valid():
            # here we are taking address, email, mobile at time of order placement
            # we are not taking it from customer account table because
            # these thing can be changes
            email = addressForm.cleaned_data['Email']
            mobile=addressForm.cleaned_data['Mobile']
            address = addressForm.cleaned_data['Address']
            #for showing total price on payment page.....accessing id from cookies then fetching  price of product from db
            total=0
            if 'product_ids' in request.COOKIES:
                product_ids = request.COOKIES['product_ids']
                if product_ids != "":
                    product_id_in_cart=product_ids.split('|')
                    products=models.Product.objects.all().filter(id__in = product_id_in_cart)
                    for p in products:
                        total=total+p.price

            response = render(request, 'customer/payment.html',{'total':total})
            response.set_cookie('email',email)
            response.set_cookie('mobile',mobile)
            response.set_cookie('address',address)
            return response
    return render(request,'customer/customer_address.html',{'addressForm':addressForm,'product_in_cart':product_in_cart,'product_count_in_cart':product_count_in_cart})




# here we are just directing to this view...actually we have to check whther payment is successful or not
#then only this view should be accessed
@login_required(login_url='customerlogin')
def payment_success_view(request):
    # Here we will place order | after successful payment
    # we will fetch customer  mobile, address, Email
    # we will fetch product id from cookies then respective details from db
    # then we will create order objects and store in db
    # after that we will delete cookies because after order placed...cart should be empty
    customer=models.Customer.objects.get(user_id=request.user.id)
    products=None
    email=None
    mobile=None
    address=None
    if 'product_ids' in request.COOKIES:
        product_ids = request.COOKIES['product_ids']
        if product_ids != "":
            product_id_in_cart=product_ids.split('|')
            products=models.Product.objects.all().filter(id__in = product_id_in_cart)
            # Here we get products list that will be ordered by one customer at a time

    # these things can be change so accessing at the time of order...
    if 'email' in request.COOKIES:
        email=request.COOKIES['email']
    if 'mobile' in request.COOKIES:
        mobile=request.COOKIES['mobile']
    if 'address' in request.COOKIES:
        address=request.COOKIES['address']

    # here we are placing number of orders as much there is a products
    # suppose if we have 5 items in cart and we place order....so 5 rows will be created in orders table
    # there will be lot of redundant data in orders table...but its become more complicated if we normalize it
    for product in products:
        models.Orders.objects.get_or_create(customer=customer,product=product,status='Pending',email=email,mobile=mobile,address=address)

    # after order placed cookies should be deleted
    response = render(request,'customer/payment_success.html')
    response.delete_cookie('product_ids')
    response.delete_cookie('email')
    response.delete_cookie('mobile')
    response.delete_cookie('address')
    return response




@login_required(login_url='customerlogin')
@user_passes_test(is_customer)
#for order status, which is managed by the business
def my_order_view(request):
    customer=models.Customer.objects.get(user_id=request.user.id)
    orders=models.Orders.objects.all().filter(customer_id = customer)
    ordered_products=[]
    for order in orders:
        ordered_product=models.Product.objects.all().filter(id=order.product.id)
        ordered_products.append(ordered_product)

    return render(request,'customer/my_order.html',{'data':zip(ordered_products,orders)})




#--------------for order invoice bill download and printing in pdf


def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    html  = template.render(context_dict)
    result = io.BytesIO()
    pdf = pisa.pisaDocument(io.BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return

@login_required(login_url='customerlogin')
@user_passes_test(is_customer)
def download_invoice_view(request,orderID,productID):
    order=models.Orders.objects.get(id=orderID)
    product=models.Product.objects.get(id=productID)
    mydict={
        'orderDate':order.order_date,
        'customerName':request.user,
        'customerEmail':order.email,
        'customerMobile':order.mobile,
        'shipmentAddress':order.address,
        'orderStatus':order.status,

        'productName':product.name,
        'productImage':product.product_image,
        'productPrice':product.price,
        'productDescription':product.description,
    }
    return render_to_pdf('customer/download_invoice.html',mydict)






@login_required(login_url='customerlogin')
@user_passes_test(is_customer)
#to manage the profile function
def my_profile_view(request):
    customer=models.Customer.objects.get(user_id=request.user.id)
    return render(request,'customer/my_profile.html',{'customer':customer})


@login_required(login_url='customerlogin')
@user_passes_test(is_customer)
#to manage the profile editing function
def edit_profile_view(request):
    customer = models.Customer.objects.get(user=request.user)
    user = request.user

    if request.method == 'POST':
        user_form = CustomerUserForm(request.POST, instance=user)
        customer_form = CustomerForm(request.POST, request.FILES, instance=customer)
        
        if user_form.is_valid() and customer_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)  # Ensure password is hashed
            user.save()
            customer = customer_form.save(commit=False)
            customer.user = user  # Associate customer with updated user
            customer.save()
            return HttpResponseRedirect('my-profile')  # Redirect to profile page after successful edit

    else:
        user_form = CustomerUserForm(instance=user)
        customer_form = CustomerForm(instance=customer)

    context = {
        'userForm': user_form,
        'customerForm': customer_form,
    }

    return render(request, 'customer/edit_profile.html', context)



@login_required(login_url='customerlogin')
def add_review(request, product_id):
    product = models.Product.objects.get(pk=product_id)
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.product = product
            review.save()
            return redirect('product-detail',pk=product_id)
    else:
        form = ReviewForm()
    return render(request, 'customer/add_review.html', {'form': form, 'product': product})


#---------------------------------------------------------------------------------
#------------------------ ABOUT US AND CONTACT US VIEWS START --------------------
#---------------------------------------------------------------------------------
def aboutus_view(request):
    return render(request,'customer/aboutus.html')

def terms_conditions_view(request):
    return render(request,'customer/terms.html')

def FAQs_view(request):
    return render(request,'customer/faq.html')

def reviews_view(request):
    reviews = Review.objects.all()
    context = {'reviews': reviews}
    return render(request,'customer/my_review.html', context)

def contactus_view(request):
    sub = forms.ContactusForm()
    if request.method == 'POST':
        sub = forms.ContactusForm(request.POST)
        if sub.is_valid():
            email = sub.cleaned_data['Email']
            name=sub.cleaned_data['Name']
            message = sub.cleaned_data['Message']
            send_mail(str(name)+' || '+str(email),message, settings.EMAIL_HOST_USER, settings.EMAIL_RECEIVING_USER, fail_silently = False)
            return render(request, 'customer/contactussuccess.html')
    return render(request, 'customer/contactus.html', {'form':sub})
