"""
WSGI config for GROUP12_SDProject project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/howto/deployment/wsgi/
"""

import sys
import os
from django.core.wsgi import get_wsgi_application

path = '/home/genericodex/softwaredevtproject_12/GROUP12_SDProject'

if path not in sys.path:
    sys.path.insert(0,path)

# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'GROUP12_SDProject.settings')
os.environ['DJANGO_SETTINGS_MODULE'] = 'GROUP12_SDProject.settings'

application = get_wsgi_application()
