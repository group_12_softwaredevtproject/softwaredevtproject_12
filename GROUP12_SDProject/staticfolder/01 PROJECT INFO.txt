**Hello, This is GROUP 12 of B.Sc. Computer Science(Day) '23/24 Yr 1, CoCIS, Makerere University**

**Developed By Group 12**

**Instructions**
- Install the Requirements: pip install -r requirements.txt
- Then, make database migrations: python manage.py makemigrations
- python manage.py migrate
- And finally, run the application: python manage.py runserver

For Admin Account, please create one with superuser!
