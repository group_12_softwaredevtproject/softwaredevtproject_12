from django.contrib import messages
from django.core.mail import send_mail,EmailMessage
from django.utils.crypto import get_random_string
from django.contrib.auth.models import User,Group
from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponseRedirect,HttpResponse
from django.contrib.auth.decorators import login_required,user_passes_test
from django.conf import settings
from django.template.loader import render_to_string
from django.db.models import Sum
from customer.models import *
from customer.forms import *
from customer.views import *
from .models import *
from .forms import *
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

sender_email = "tuhaiseugene@gmail.com"

sender_password = "xfko grfs unsc gycc"

def send_business_message(recipient_email, username):
            message = MIMEMultipart()
            message['From'] = sender_email
            message['To'] = recipient_email
            message['Subject']  = "BUSINESS EMAIL CONFIRMED!!!"
        
            body = f"""
            Warm Greetings, {username},
    
            WELCOME TO SHOPNET

            This is to congragulate you on successfully 
            creating an account with us as a business user
        
            If this was not by you please ignore this.

            The SHOPNET team
            """
            message.attach(MIMEText(body, "plain"))
        
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
        
            server.login(sender_email, sender_password)
        
            server.sendmail(sender_email, recipient_email, message.as_string())
        
            server.quit()

            return 1
#---------------------------------------------------------------------------------
#------------------------ BUSINESS RELATED VIEWS START ------------------------------
#---------------------------------------------------------------------------------


def business_signup_view(request):
    business_userForm=BusinessUserForm()
    businessForm =BusinessForm()
    mydict={'businessuserForm':business_userForm,'businessForm':businessForm}
    if request.method=='POST':
        business_userForm=BusinessUserForm(request.POST)
        businessForm =BusinessForm(request.POST,request.FILES)
        if business_userForm.is_valid() and businessForm.is_valid():
            user=business_userForm.save()
            user.set_password(user.password)
            user.save()
            business=businessForm.save(commit=False)
            business.user=user
            business.save()
            send_business_message(user.email,user.username)
            business_group = Group.objects.get_or_create(name='BUSINESS')
            business_group[0].user_set.add(user)
            return HttpResponseRedirect('businesslogin')
        else:
            messages.error(request,'Sign Up Failed!! Try again.')
    return render(request,'business/business_signup.html',context=mydict)

#-----------for checking user isbusiness
def is_business(user):
    return user.groups.filter(name='BUSINESS').exists()

@login_required(login_url='businesslogin')
def business_dashboard_view(request):
    # for cards on dashboard
    business = Business.objects.get(user=request.user)
    customercount=Customer.objects.all().count()
    products=Product.objects.filter(business=business)
    orders=Orders.objects.filter(product__in=products)
    total_earnings = orders.aggregate(total_earnings=Sum('product__price'))['total_earnings'] or 0
    # for recent order tables
    # orders=Orders.objects.all()
    ordered_products=[]
    ordered_bys=[]
    for order in orders:
        ordered_product=Product.objects.all().filter(id=order.product.id)
        ordered_by=Customer.objects.all().filter(id = order.customer.id)
        ordered_products.append(ordered_product)
        ordered_bys.append(ordered_by)

    mydict={
    'customercount':customercount,
    'productcount':products.count(),
    'ordercount':orders.count(),
    'business_pic':business.business_pic.url,
    'total_earnings': total_earnings,
    'data':zip(ordered_products,ordered_bys,orders),
    }
    return render(request,'business/business_dashboard.html',context=mydict)

# business view customer table
@login_required(login_url='businesslogin')
def business_view_customer_view(request):
    customers=Customer.objects.all()
    return render(request,'business/view_customer.html',{'customers':customers})

# admin delete customer
@login_required(login_url='businesslogin')
def business_delete_customer_view(request,pk):
    customer=Customer.objects.get(id=pk)
    user=User.objects.get(id=customer.user_id)
    user.delete()
    customer.delete()
    return redirect('view-customer')


@login_required(login_url='businesslogin')
def business_update_customer_view(request,pk):
    customer=Customer.objects.get(id=pk)
    user=User.objects.get(id=customer.user_id)
    userForm=CustomerUserForm(instance=user)
    customerForm=CustomerForm(request.FILES,instance=customer)
    mydict={'userForm':userForm,'customerForm':customerForm}
    if request.method=='POST':
        userForm=CustomerUserForm(request.POST,instance=user)
        customerForm=CustomerForm(request.POST,instance=customer)
        if userForm.is_valid() and customerForm.is_valid():
            user=userForm.save()
            user.set_password(user.password)
            user.save()
            customerForm.save()
            return redirect('view-customer')
    return render(request,'business/business_update_customer.html',context=mydict)

# admin view the product
@login_required(login_url='businesslogin')
def business_products_view(request):
    products=Product.objects.filter(business__user=request.user)
    return render(request,'business/business_products.html',{'products':products})


# admin add product by clicking on floating button
@login_required(login_url='businesslogin')
def business_add_product_view(request):
    if request.method=='POST':
        productForm=ProductForm(request.POST, request.FILES)
        if productForm.is_valid():
            product = productForm.save(commit=False)
            business = Business.objects.get(user=request.user)
            product.business = business
            product.save()
        return HttpResponseRedirect('business-products')
    else:
        productForm=ProductForm()
    return render(request,'business/business_add_products.html',{'productForm':productForm})


@login_required(login_url='businesslogin')
def business_delete_product_view(request,pk):
    product=get_object_or_404(Product,id=pk, business__user=request.user)
    # if product.business == request.user:
    product.delete()
    return redirect('business-products')


@login_required(login_url='businesslogin')
def business_update_product_view(request,pk):
    product=get_object_or_404(Product,id=pk, business__user=request.user)
    productForm=ProductForm(instance=product)
    if request.method=='POST':
        productForm=ProductForm(request.POST,request.FILES,instance=product)
        if productForm.is_valid():
            productForm.save()
            return redirect('business-products')
    return render(request,'business/business_update_product.html',{'productForm':productForm})


@login_required(login_url='businesslogin')
def business_view_booking_view(request):
    business = Business.objects.get(user=request.user)
    products=Product.objects.filter(business=business)
    orders=Orders.objects.filter(product__in=products)
    ordered_products=[]
    ordered_bys=[]
    for order in orders:
        ordered_product=Product.objects.all().filter(id=order.product.id)
        ordered_by=Customer.objects.all().filter(id = order.customer.id)
        ordered_products.append(ordered_product)
        ordered_bys.append(ordered_by)
    return render(request,'business/business_view_booking.html',{'data':zip(ordered_products,ordered_bys,orders)})


@login_required(login_url='businesslogin')
def business_delete_order_view(request,pk):
    order=Orders.objects.get(id=pk)
    order.delete()
    return redirect('business-view-booking')

# for changing status of order (pending,delivered...)
@login_required(login_url='businesslogin')
def business_update_order_view(request,pk):
    order=Orders.objects.get(id=pk)
    orderForm=OrderForm(instance=order)
    if request.method=='POST':
        orderForm=OrderForm(request.POST,instance=order)
        if orderForm.is_valid():
            orderForm.save()
            return redirect('business-view-booking')
    return render(request,'business/update_order.html',{'orderForm':orderForm})


# admin view the feedback
@login_required(login_url='businesslogin')
def business_view_feedback_view(request):
    feedbacks=Feedback.objects.all().order_by('-id')
    return render(request,'business/view_feedback.html',{'feedbacks':feedbacks})


@login_required(login_url='businesslogin')
@user_passes_test(is_business)
def business_profile_view(request):
    business=Business.objects.get(user_id=request.user.id)
    return render(request,'business/business_profile.html',{'business':business})


@login_required(login_url='businesslogin')
@user_passes_test(is_business)
def business_edit_profile_view(request):
    business = Business.objects.get(user=request.user)
    user = request.user

    if request.method == 'POST':
        businessuserform = BusinessUserForm(request.POST, instance=user)
        businessform = BusinessForm(request.POST, request.FILES, instance=business)
        
        if businessuserform.is_valid() and businessform.is_valid():
            user = businessuserform.save()
            user.set_password(user.password)  # Ensure password is hashed
            user.save()
            business = businessform.save(commit=False)
            business.user = user  # Associate customer with updated user
            business.save()
            return HttpResponseRedirect('business-profile')  # Redirect to profile page after successful edit

    else:
        businessuserform = BusinessUserForm(instance=user)
        businessform = BusinessForm(instance=business)

    context = {
        'businessuserForm': businessuserform,
        'businessForm': businessform,
    }

    return render(request, 'business/edit_business_profile.html', context)

