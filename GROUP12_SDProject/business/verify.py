import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from django.conf import settings

sender_email = settings.EMAIL_HOST_USER
sender_password = settings.EMAIL_HOST_PASSWORD


def send_message(first_name, recipient_email, token, host):
        message = MIMEMultipart()
        message['From'] = sender_email
        message['To'] = recipient_email
        message['Subject']  = "SIGNUP | SHOPPIE"
        
        body = f"""
        Warm Greetings {first_name},
        Please use this link to activate your account:
                 
        
        http://{host}/auth/activate/{token}   
            
    
        You have 24 hours before this account is deleted.
    
        THE SHOPPIE TEAM WELCOMES YOU🎊
        
        If this was not by you please ignore this.
        """
        message.attach(MIMEText(body, "plain"))
        
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        
        server.login(sender_email, sender_password)
        
        server.sendmail(sender_email, recipient_email, message.as_string())
        
        server.quit()
        
        return 1