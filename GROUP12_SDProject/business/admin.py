from django.contrib import admin
from .models import Product,Business,Category
from django.utils.html import format_html

class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'business', 'category', 'price', 'description','product_image_thumbnail')
    search_fields = ('name', 'description')
    list_filter = ('category',)

    def product_image_thumbnail(self, obj):
        return format_html(f'<img src="/static{obj.product_image.url}" style="width: 50px; height:50px;"/>') if obj.product_image else ''
    product_image_thumbnail.short_description = 'Product Image'
admin.site.register(Product, ProductAdmin)

class BusinessAdmin(admin.ModelAdmin):
    list_display = ('get_name', 'businessname', 'address', 'mobile', 'created_at', 'get_email','business_e_mail','business_pic_thumbnail')
    search_fields = ('user__first_name', 'user__last_name', 'businessname')

    def get_email(self, obj):
        return obj.user.email
    get_email.short_description = 'Email'

    def business_pic_thumbnail(self, obj):
        return format_html(f'<img src="/static{obj.business_pic.url}" style="width: 50px; height:50px;"/>') if obj.business_pic else ''
    business_pic_thumbnail.short_description = 'Business Image'
admin.site.register(Business, BusinessAdmin)

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title',)
    def category_pic_thumbnail(self, obj):
        return format_html(f'<img src="/static{obj.category_pic.url}" style="width: 50px; height:50px;"/>') if obj.category_pic else ''
    category_pic_thumbnail.short_description = 'Category Image'
admin.site.register(Category, CategoryAdmin)

