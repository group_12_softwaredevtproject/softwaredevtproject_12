# Generated by Django 4.2.5 on 2024-04-27 11:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('business', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='business',
            options={'verbose_name_plural': 'Businesses'},
        ),
        migrations.AddField(
            model_name='business',
            name='business_e_mail',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
        migrations.AddField(
            model_name='category',
            name='category_pic',
            field=models.ImageField(blank=True, default='profile_pic/product.png', null=True, upload_to='profile_pic/CategoryPic/'),
        ),
        migrations.AlterField(
            model_name='business',
            name='business_pic',
            field=models.ImageField(blank=True, default='profile_pic/user-default.png', null=True, upload_to='profile_pic/BusinessProfilePic/'),
        ),
        migrations.AlterField(
            model_name='business',
            name='mobile',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='business',
            name='user',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='product',
            name='description',
            field=models.CharField(blank=True, max_length=40, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_image',
            field=models.ImageField(blank=True, default='profile_pic/product.png', null=True, upload_to='product_image/'),
        ),
    ]
