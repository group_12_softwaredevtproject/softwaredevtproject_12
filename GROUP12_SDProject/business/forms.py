from django import forms
from django.contrib.auth.models import User
from . import models

class ProductForm(forms.ModelForm):
    class Meta:
        model=models.Product
        fields=['name','category','price','description','product_image']

class BusinessUserForm(forms.ModelForm):
    password_confirm = forms.CharField(label='Confirm Password', widget=forms.PasswordInput)
    class Meta:
        model=User
        fields=['first_name','last_name','username','email','password']
        widgets = {
        'password': forms.PasswordInput()
        }
    def clean(self):
            cleaned_data = super().clean()
            password = cleaned_data.get('password')
            password_confirm = cleaned_data.get('password_confirm')
    
            if password != password_confirm:
                raise forms.ValidationError("Passwords do not match")
    
            return cleaned_data
        
class BusinessForm(forms.ModelForm):
    class Meta:
        model=models.Business
        fields=['business_e_mail','address','mobile','business_pic']

class PasswordResetRequestForm(forms.Form):
    email = forms.EmailField()

class PasswordResetForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)